# Nextjs Single Page Application

**An old SPA that parses the `collectionapi.metmuseum.org` api**

Serves as an example for handling:
- `logins`
- `Server side rendering`
- `Static site generation`
- `data fetching with useSWR`
- `shared state with jotai`
- `guarding routes`
- `creating react components`
- `using react-bootstrap components`

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000)
